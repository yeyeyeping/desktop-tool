import Vue from "vue";
import {
  Table,
  TableColumn,
  Button,
  ButtonGroup,
  Row,
  Progress,
  Calendar,
  DatePicker,
  Drawer,
  Timeline,
  TimelineItem,
  Card,
  Tooltip,
  ColorPicker
} from "element-ui";
Vue.prototype.$ELEMENT = { size: "small", zIndex: 3000 };
Vue.use(Table);
Vue.use(TableColumn);
Vue.use(Button);
Vue.use(ButtonGroup);
Vue.use(Row);
Vue.use(Progress);
Vue.use(Calendar);
Vue.use(DatePicker);
Vue.use(Drawer);
Vue.use(Timeline);
Vue.use(TimelineItem);
Vue.use(Card);
Vue.use(Tooltip);
Vue.use(ColorPicker);