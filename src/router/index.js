import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Calendar",
    redirect: "/can"
  },
  {
    path: "/todo",
    name: "Timeline",
    component: () => import("../views/MyTimeline.vue")
  },
  {
    path: "/done",
    name: "Done",
    component: () => import("../views/Done.vue")
  },
  {
    path: "/shedule",
    name: "shedule",
    component: () => import("../views/Schedule.vue")
  },
  {
    path: "/wallfall",
    name: "wallfall",
    component: () => import("../views/WallFall.vue"),
    children: [
      {
        path: "wallpaper",
        name: "WallPaper",
        component: () => import("../components/WallPaper.vue")
      }
    ]
  },
  {
    path: "/icon",
    name: "icon",
    component: () => import("../views/IconContainer.vue")
  },
  {
    path: "/music",
    name: "Music",
    component: () => import("../views/MusicPlayer.vue")
  },
  {
    path: "/can",
    name: "Calendar",
    component: () => import("../views/MyCalendar.vue")
  }
];

const router = new VueRouter({
  routes
});

export default router;
